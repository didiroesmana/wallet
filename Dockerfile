FROM golang:1.16-alpine as builder

# git is required to fetch go dependencies
RUN apk add --no-cache ca-certificates git

ARG VERSION

# Create a netrc file using the credentials specified using --build-arg
ARG WORKDIR=/go/src/bitbucket.org/didiroesmana/wallet
WORKDIR ${WORKDIR}
# Fetch dependencies first; they are less susceptible to change on every build
# and will therefore be cached for speeding up the next build 

ARG GO111MODULE=auto
ENV GO111MODULE=${GO111MODULE} 

# encourage people to use Go v1.11 and beyond
COPY go.mod . 
COPY go.sum .
RUN if [ "$GO111MODULE" = "on" ] ; then go mod download ; fi

COPY . .
# Import the code from the context.
RUN CGO_ENABLED=0 go install -installsuffix "static" ./cmd/httpserver


FROM alpine
# these ARGs are passed from the Makefile when building the container
ARG VERSION=1.0.0
ARG ENTRYPOINT=/bin/httpserver
# we use ENV so we can use the passed ARGs in ENTRYPOINT
ENV VERSION=${VERSION} 
ENV ENTRYPOINT=${ENTRYPOINT} 
RUN apk add --no-cache ca-certificates tzdata
COPY --from=builder /go/bin /bin
USER nobody:nobody
ENTRYPOINT exec "${ENTRYPOINT}" -version "${VERSION}"