test:
	go test -cover -p=1 -count=1 ./...

# The default docker image name 
IMAGE := $(shell basename $(CURDIR))

# This version-strategy uses git tags to set the version string
VERSION := $(shell git describe --tags --always --dirty)
GONOSUMDB := $(shell echo $(GONOSUMDB))
GOPRIVATE := $(shell echo $(GOPRIVATE))

image:
	@echo "building the image... ${IMAGE}:${VERSION}"
	@docker build \
	--build-arg "GO111MODULE=on" \
	--build-arg "ENTRYPOINT=/bin/httpserver" \
	--build-arg "WORKDIR=/go/src/bitbucket.org/didiroesmana/$(IMAGE)" \
	--build-arg "VERSION=$(VERSION)" \
	-t $(IMAGE):$(VERSION) .