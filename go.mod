module bitbucket.org/didiroesmana/wallet

go 1.16

require (
	github.com/go-kit/kit v0.12.0
	github.com/google/go-cmp v0.5.7
	github.com/gorilla/mux v1.8.0
	github.com/jackc/pgx/v4 v4.14.1 // indirect
	github.com/lib/pq v1.10.4
	github.com/stretchr/objx v0.3.0 // indirect
	github.com/stretchr/testify v1.7.0
	golang.org/x/crypto v0.0.0-20220210151621-f4118a5b28e2 // indirect
	gorm.io/driver/postgres v1.2.3
	gorm.io/gorm v1.22.5
)
