package account

import (
	"context"

	"bitbucket.org/didiroesmana/wallet/pkg/entity"
)

// Service is the interface
// that provides the methods for account
type Service interface {
	Create(ctx context.Context, account *entity.Account) error
	GetByID(ctx context.Context, id string) (*entity.Account, error)
	List(ctx context.Context, page, size int) ([]*entity.Account, error)
}

// Repository is the interface
// that ensures the data persistence
type Repository interface {
	Create(ctx context.Context, account *entity.Account) error
	GetByID(ctx context.Context, id string) (*entity.Account, error)
	List(ctx context.Context, page, size int) ([]*entity.Account, error)
	Update(ctx context.Context, account *entity.Account) error
}
