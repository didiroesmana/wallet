package postgresql

import (
	"gorm.io/gorm"
)

type Account struct {
	gorm.Model
	ID       string  `gorm:"type:varchar(20);primary_key"`
	Balance  float64 `gorm:"type:decimal(20,2);default:0"`
	Currency string  `gorm:"type:varchar(10);default:'USD'"`
}
