package postgresql

import (
	"context"
	"database/sql"

	"gorm.io/driver/postgres"

	"bitbucket.org/didiroesmana/wallet/internal/account"
	err "bitbucket.org/didiroesmana/wallet/pkg/error"
	"bitbucket.org/didiroesmana/wallet/pkg/helper/pagination"

	"bitbucket.org/didiroesmana/wallet/pkg/entity"
	"gorm.io/gorm"
)

type repository struct {
	db *gorm.DB
}

func New(db *sql.DB) account.Repository {
	gormDB, _ := gorm.Open(postgres.New(postgres.Config{
		Conn: db,
	}))

	gormDB.AutoMigrate(Account{})
	return &repository{gormDB}
}

func (r *repository) Create(ctx context.Context, account *entity.Account) error {
	if r.db.Create(&Account{
		ID:       account.ID,
		Balance:  account.Balance,
		Currency: account.Currency,
	}).Error != nil {
		return err.ErrCreateFailed
	}
	return nil
}

func (r *repository) GetByID(ctx context.Context, id string) (*entity.Account, error) {
	acc := &Account{}
	if r.db.First(&acc, "id = ?", id).Error != nil {
		return nil, err.ErrAccNotFound
	}

	return &entity.Account{
		ID:       acc.ID,
		Balance:  acc.Balance,
		Currency: acc.Currency,
	}, nil
}

func (r *repository) List(ctx context.Context, page, size int) ([]*entity.Account, error) {
	accs := []*Account{}
	if r.db.Scopes(pagination.Paginate(page, size)).Find(&accs).Error != nil {
		return nil, err.ErrGetFailed
	}

	result := []*entity.Account{}
	// map accs to entity
	for _, acc := range accs {
		result = append(result, &entity.Account{
			ID:       acc.ID,
			Balance:  acc.Balance,
			Currency: acc.Currency,
		})
	}
	return result, nil
}

func (r *repository) Update(ctx context.Context, account *entity.Account) error {
	acc := &Account{}
	if r.db.First(&acc, "id = ?", account.ID).Error != nil {
		return err.ErrAccNotFound
	}
	// update account
	acc.Balance = account.Balance
	acc.Currency = account.Currency
	if r.db.Save(&acc).Error != nil {
		return err.ErrUpdateFailed
	}
	return nil
}

func Paginate(page, pageSize int) func(db *gorm.DB) *gorm.DB {
	return func(db *gorm.DB) *gorm.DB {
		if page == 0 {
			page = 1
		}

		switch {
		case pageSize > 100:
			pageSize = 100
		case pageSize <= 0:
			pageSize = 10
		}

		offset := (page - 1) * pageSize
		return db.Offset(offset).Limit(pageSize)
	}
}
