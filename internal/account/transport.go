package account

import (
	"context"
	"encoding/json"
	"net/http"

	e "bitbucket.org/didiroesmana/wallet/pkg/error"
	"bitbucket.org/didiroesmana/wallet/pkg/helper/httprequest"
	kithttp "github.com/go-kit/kit/transport/http"
	"github.com/gorilla/mux"
)

func MakeHandler(s Service) http.Handler {
	opts := []kithttp.ServerOption{
		kithttp.ServerErrorEncoder(encodeError),
	}

	createAccountHandler := kithttp.NewServer(
		MakeAccountEndpoint(s),
		decodeCreateAccountRequest,
		encodeResponse,
		opts...,
	)

	getAccountByIDHandler := kithttp.NewServer(
		MakeGetAccountByIDEndpoint(s),
		decodeGetAccountByIDRequest,
		encodeResponse,
		opts...,
	)

	listAccountsHandler := kithttp.NewServer(
		MakeListAccountsEndpoint(s),
		decodeListAccountsRequest,
		encodeResponse,
		opts...,
	)

	r := mux.NewRouter()
	r.Handle("/v1/accounts", createAccountHandler).Methods("POST")
	r.Handle("/v1/accounts", listAccountsHandler).Methods("GET")
	r.Handle("/v1/accounts/{id}", getAccountByIDHandler).Methods("GET")

	return r
}

func decodeCreateAccountRequest(_ context.Context, r *http.Request) (interface{}, error) {
	createRequest := &createAccountRequest{}
	if err := json.NewDecoder(r.Body).Decode(&createRequest); err != nil {
		return nil, err
	}

	return createRequest, nil
}

func decodeGetAccountByIDRequest(_ context.Context, r *http.Request) (interface{}, error) {
	vars := mux.Vars(r)
	id, ok := vars["id"]
	if !ok {
		return nil, e.ErrBadRequest
	}
	return &getAccountByIDRequest{ID: id}, nil
}

func decodeListAccountsRequest(_ context.Context, r *http.Request) (interface{}, error) {
	p, s := httprequest.ParsePagination(r)
	return &listAccountsRequest{p, s}, nil
}

func encodeResponse(ctx context.Context, w http.ResponseWriter, response interface{}) error {
	if e, ok := response.(errorer); ok && e.error() != nil {
		encodeError(ctx, e.error(), w)
		return nil
	}
	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	return json.NewEncoder(w).Encode(response)
}

type errorer interface {
	error() error
}

// encode errors from business-logic
func encodeError(_ context.Context, err error, w http.ResponseWriter) {
	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	switch err {
	case e.ErrAccNotFound:
		w.WriteHeader(http.StatusNotFound)
	case e.ErrInvalidArgument:
		w.WriteHeader(http.StatusBadRequest)
	default:
		w.WriteHeader(http.StatusInternalServerError)
	}
	json.NewEncoder(w).Encode(map[string]interface{}{
		"error": err.Error(),
	})
}
