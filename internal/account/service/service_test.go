package service

import (
	"context"
	"testing"

	"bitbucket.org/didiroesmana/wallet/pkg/entity"
	e "bitbucket.org/didiroesmana/wallet/pkg/error"
	am "bitbucket.org/didiroesmana/wallet/pkg/mocks/account"
	"github.com/google/go-cmp/cmp"
)

func TestGetByID(t *testing.T) {
	accRepo := &am.Repository{}

	type args struct {
		ctx context.Context
		id  string
	}
	tests := []struct {
		name        string
		args        args
		want        *entity.Account
		mockUser    func()
		wantErr     bool
		expectedErr error
	}{
		{
			name: "success",
			args: args{
				ctx: context.Background(),
				id:  "test",
			},
			want: &entity.Account{
				ID: "test",
			},
			mockUser: func() {
				accRepo.On("GetByID", context.Background(), "test").Return(&entity.Account{ID: "test"}, nil)
			},
			wantErr: false,
		},
		{
			name: "not found",
			args: args{
				ctx: context.Background(),
				id:  "test-1",
			},
			want: nil,
			mockUser: func() {
				accRepo.On("GetByID", context.Background(), "test-1").Return(nil, e.ErrAccNotFound)
			},
			wantErr:     true,
			expectedErr: e.ErrAccNotFound,
		},
	}

	svc := New(accRepo)

	t.Run("TestGetByID", func(t *testing.T) {
		for _, tt := range tests {
			t.Run(tt.name, func(t *testing.T) {
				tt.mockUser()
				got, err := svc.GetByID(tt.args.ctx, tt.args.id)
				if (err != nil) != tt.wantErr {
					t.Errorf("GetByID() error = %v, wantErr %v", err, tt.wantErr)
				}
				compRes := cmp.Diff(got, tt.want)

				if compRes != "" {
					t.Fatalf("Case %s expected and res not same [%v]", tt.name, compRes)
				}
			})
		}
	})

}

func TestCreate(t *testing.T) {
	accRepo := &am.Repository{}

	type args struct {
		ctx context.Context
		acc *entity.Account
	}
	tests := []struct {
		name        string
		args        args
		mockUser    func()
		wantErr     bool
		expectedErr error
	}{
		{
			name: "success",
			args: args{
				ctx: context.Background(),
				acc: &entity.Account{
					ID:      "test",
					Balance: 100,
				},
			},
			mockUser: func() {
				accRepo.On("GetByID", context.Background(), "test").Return(nil, e.ErrAccNotFound)
				accRepo.On("Create", context.Background(), &entity.Account{ID: "test", Balance: 100}).Return(nil)
			},
			wantErr:     false,
			expectedErr: nil,
		},
		{
			name: "failed_user_exist",
			args: args{
				ctx: context.Background(),
				acc: &entity.Account{
					ID:      "test-1",
					Balance: 100,
				},
			},
			mockUser: func() {
				accRepo.On("GetByID", context.Background(), "test-1").Return(&entity.Account{ID: "test-1"}, nil)
			},
			wantErr:     true,
			expectedErr: e.ErrAccountExist,
		},
		{
			name: "failed_balance_below_zero",
			args: args{
				ctx: context.Background(),
				acc: &entity.Account{
					ID:      "test-2",
					Balance: 0,
				},
			},
			mockUser: func() {
			},
			wantErr:     true,
			expectedErr: e.ErrAccBalanceMin,
		},
	}

	svc := New(accRepo)

	t.Run("TestCreate", func(t *testing.T) {
		for _, tt := range tests {
			t.Run(tt.name, func(t *testing.T) {
				tt.mockUser()
				err := svc.Create(tt.args.ctx, tt.args.acc)
				if err != nil && tt.expectedErr != nil {
					if err.Error() != tt.expectedErr.Error() {
						t.Errorf("Create = %v, want %v", err, tt.expectedErr)
					}
				}
			})
		}
	})

}

func TestList(t *testing.T) {
	accRepo := &am.Repository{}

	type args struct {
		ctx  context.Context
		size int
		page int
	}

	// test cases
	tests := []struct {
		name        string
		args        args
		want        []*entity.Account
		mockUser    func()
		wantErr     bool
		expectedErr error
	}{
		{
			name: "success",
			args: args{
				ctx:  context.Background(),
				size: 10,
				page: 1,
			},
			want: []*entity.Account{
				{
					ID: "test-1",
				},
				{
					ID: "test-2",
				},
			},
			mockUser: func() {
				accRepo.On("List", context.Background(), 1, 10).Return([]*entity.Account{
					{
						ID: "test-1",
					},
					{
						ID: "test-2",
					},
				}, nil)
			},
			wantErr:     false,
			expectedErr: nil,
		},
		{
			name: "success_page_below_zero",
			args: args{
				ctx:  context.Background(),
				size: 11,
				page: -1,
			},
			want: []*entity.Account{
				{
					ID: "test-1",
				},
			},
			mockUser: func() {
				accRepo.On("List", context.Background(), 1, 11).Return([]*entity.Account{
					{
						ID: "test-1",
					},
				}, nil)
			},
			wantErr:     false,
			expectedErr: nil,
		},
		{
			name: "success_size_below_zero",
			args: args{
				ctx:  context.Background(),
				size: -1,
				page: 2,
			},
			want: []*entity.Account{
				{
					ID: "test-1",
				},
			},
			mockUser: func() {
				accRepo.On("List", context.Background(), 2, 10).Return([]*entity.Account{
					{
						ID: "test-1",
					},
				}, nil)
			},
		},
		{
			name: "success_size_above_100",
			args: args{
				ctx:  context.Background(),
				size: 101,
				page: 3,
			},
			want: []*entity.Account{
				{
					ID: "test-1",
				},
			},
			mockUser: func() {
				accRepo.On("List", context.Background(), 3, 10).Return([]*entity.Account{
					{
						ID: "test-1",
					},
				}, nil)
			},
		},
	}

	svc := New(accRepo)

	t.Run("TestList", func(t *testing.T) {
		for _, tt := range tests {
			t.Run(tt.name, func(t *testing.T) {
				tt.mockUser()
				got, err := svc.List(tt.args.ctx, tt.args.page, tt.args.size)
				if (err != nil) != tt.wantErr {
					t.Errorf("List() error = %v, wantErr %v", err, tt.wantErr)
				}
				compRes := cmp.Diff(got, tt.want)

				if compRes != "" {
					t.Fatalf("Case %s expected and res not same [%v]", tt.name, compRes)
				}
			})
		}
	})
}
