package service

import (
	"context"

	"bitbucket.org/didiroesmana/wallet/internal/account"
	"bitbucket.org/didiroesmana/wallet/pkg/entity"
	e "bitbucket.org/didiroesmana/wallet/pkg/error"
)

type service struct {
	repo account.Repository
}

func New(repo account.Repository) account.Service {
	return &service{
		repo: repo,
	}
}

func (s *service) Create(ctx context.Context, account *entity.Account) error {
	if account.Balance <= 0 {
		return e.ErrAccBalanceMin
	}

	// check if user is exist by its id
	if acc, err := s.repo.GetByID(ctx, account.ID); err == nil && acc != nil {
		return e.ErrAccountExist
	}
	return s.repo.Create(ctx, account)
}

func (s *service) GetByID(ctx context.Context, id string) (*entity.Account, error) {
	return s.repo.GetByID(ctx, id)
}

func (s *service) List(ctx context.Context, page, size int) ([]*entity.Account, error) {
	if size <= 0 || size > 100 {
		size = 10
	}

	if page <= 0 {
		page = 1
	}

	return s.repo.List(ctx, page, size)
}
