package account

import (
	"context"

	"bitbucket.org/didiroesmana/wallet/pkg/entity"
	"github.com/go-kit/kit/endpoint"
)

type createAccountRequest struct {
	entity.Account
}

type createAccountResponse struct {
	entity.Account
}

type getAccountByIDRequest struct {
	ID string
}

type getAccountByIDResponse struct {
	entity.Account
}

type listAccountsRequest struct {
	Page int
	Size int
}

type listAccountsResponse struct {
	Accounts []*entity.Account `json:"accounts"`
}

func MakeAccountEndpoint(s Service) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(*createAccountRequest)
		acc := &entity.Account{
			ID:       req.ID,
			Balance:  req.Balance,
			Currency: req.Currency,
		}
		err := s.Create(ctx, acc)
		if err != nil {
			return nil, err
		}

		return createAccountResponse{*acc}, nil
	}
}

func MakeGetAccountByIDEndpoint(s Service) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(*getAccountByIDRequest)

		acc, err := s.GetByID(ctx, req.ID)
		if err != nil {
			return nil, err
		}

		return getAccountByIDResponse{*acc}, nil
	}
}

func MakeListAccountsEndpoint(s Service) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(*listAccountsRequest)
		accs, err := s.List(ctx, req.Page, req.Size)
		if err != nil {
			return nil, err
		}

		return listAccountsResponse{accs}, nil
	}
}
