package postgresql

import (
	"gorm.io/gorm"
)

type Transaction struct {
	gorm.Model
	Amount    float64 `gorm:"type:decimal(20,2);default:0"`
	Account   string  `gorm:"type:varchar(10);default:''"`
	From      string  `gorm:"type:varchar(10);default:''"`
	To        string  `gorm:"type:varchar(10);default:''"`
	Direction string  `gorm:"type:varchar(10);default:''"`
}
