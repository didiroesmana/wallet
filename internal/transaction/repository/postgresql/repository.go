package postgresql

import (
	"context"
	"database/sql"

	"bitbucket.org/didiroesmana/wallet/internal/transaction"
	"bitbucket.org/didiroesmana/wallet/pkg/entity"
	err "bitbucket.org/didiroesmana/wallet/pkg/error"
	"bitbucket.org/didiroesmana/wallet/pkg/helper/pagination"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
)

type repository struct {
	db *gorm.DB
}

func New(db *sql.DB) transaction.Repository {
	gormDB, _ := gorm.Open(postgres.New(postgres.Config{
		Conn: db,
	}))

	gormDB.AutoMigrate(Transaction{})
	return &repository{gormDB}
}

func (r *repository) List(ctx context.Context, page, size int) ([]*entity.Transaction, error) {
	txs := []*Transaction{}

	if r.db.Scopes(pagination.Paginate(page, size)).Find(&txs).Error != nil {
		return nil, err.ErrGetFailed
	}

	result := []*entity.Transaction{}
	for _, tx := range txs {
		result = append(result, &entity.Transaction{
			Amount:      tx.Amount,
			Account:     tx.Account,
			ToAccount:   tx.To,
			FromAccount: tx.From,
			Direction:   tx.Direction,
		})
	}

	return result, nil
}

func (r *repository) Create(ctx context.Context, tx *entity.Transaction) error {
	if r.db.Create(&Transaction{
		Account:   tx.Account,
		Amount:    tx.Amount,
		To:        tx.ToAccount,
		From:      tx.FromAccount,
		Direction: tx.Direction,
	}).Error != nil {
		return err.ErrCreateFailed
	}
	return nil
}
