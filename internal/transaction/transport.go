package transaction

import (
	"context"
	"encoding/json"
	"net/http"

	e "bitbucket.org/didiroesmana/wallet/pkg/error"
	"bitbucket.org/didiroesmana/wallet/pkg/helper/httprequest"
	kithttp "github.com/go-kit/kit/transport/http"
	"github.com/gorilla/mux"
)

func MakeHandler(svc Service) http.Handler {
	opts := []kithttp.ServerOption{
		kithttp.ServerErrorEncoder(encodeError),
	}

	listTransactionHandler := kithttp.NewServer(
		MakeListTransactionEndpoint(svc),
		decodeListTransactionRequest,
		encodeResponse,
		opts...,
	)

	transferTransactionHandler := kithttp.NewServer(
		MakeTransferTransactionEndpoint(svc),
		decodeTransferTransactionRequest,
		encodeResponse,
		opts...,
	)

	r := mux.NewRouter()
	r.Handle("/v1/transactions", listTransactionHandler).Methods("GET")
	r.Handle("/v1/transactions", transferTransactionHandler).Methods("POST")

	return r
}

func decodeListTransactionRequest(_ context.Context, r *http.Request) (interface{}, error) {
	p, s := httprequest.ParsePagination(r)
	return &listTransactionRequest{p, s}, nil
}

func decodeTransferTransactionRequest(_ context.Context, r *http.Request) (interface{}, error) {
	tr := &transferTransactionRequest{}
	if err := json.NewDecoder(r.Body).Decode(&tr); err != nil {
		return nil, err
	}

	if tr.From == "" || tr.To == "" || tr.Amount <= 0 {
		return nil, e.ErrInvalidRequest
	}

	return tr, nil
}

func encodeResponse(ctx context.Context, w http.ResponseWriter, response interface{}) error {
	if e, ok := response.(errorer); ok && e.error() != nil {
		encodeError(ctx, e.error(), w)
		return nil
	}
	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	return json.NewEncoder(w).Encode(response)
}

type errorer interface {
	error() error
}

// encode errors from business-logic
func encodeError(_ context.Context, err error, w http.ResponseWriter) {
	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	switch err {
	case e.ErrAccNotFound:
		w.WriteHeader(http.StatusNotFound)
	case e.ErrInvalidArgument, e.ErrAccBalanceMin, e.ErrInvalidRequest:
		w.WriteHeader(http.StatusBadRequest)
	default:
		w.WriteHeader(http.StatusInternalServerError)
	}
	json.NewEncoder(w).Encode(map[string]interface{}{
		"error": err.Error(),
	})
}
