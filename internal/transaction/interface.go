package transaction

import (
	"context"

	"bitbucket.org/didiroesmana/wallet/pkg/entity"
)

// Service is the interface
// that provides the methods for transaction
type Service interface {
	// List returns a list of transactions
	List(ctx context.Context, page, size int) ([]*entity.Transaction, error)
	// Transfer transfers money from one account to another
	Transfer(ctx context.Context, amount float64, to, from string) error
}

// Repository is the interface
// that ensures the data persistence
type Repository interface {
	List(ctx context.Context, page, size int) ([]*entity.Transaction, error)
	Create(ctx context.Context, trx *entity.Transaction) error
}
