package transaction

import (
	"context"

	"bitbucket.org/didiroesmana/wallet/pkg/entity"
	e "bitbucket.org/didiroesmana/wallet/pkg/error"
	"github.com/go-kit/kit/endpoint"
)

type listTransactionRequest struct {
	Page int `json:"page"`
	Size int `json:"size"`
}

type listTransactionResponse struct {
	Transactions []*entity.Transaction `json:"transactions"`
}

type transferTransactionRequest struct {
	Amount float64 `json:"amount"`
	To     string  `json:"to"`
	From   string  `json:"from"`
}

type transaferTransactionResponse struct {
	Message string `json:"message"`
}

func MakeListTransactionEndpoint(s Service) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(*listTransactionRequest)
		trxs, err := s.List(ctx, req.Page, req.Size)
		if err != nil {
			return nil, err
		}

		return listTransactionResponse{Transactions: trxs}, nil
	}
}

func MakeTransferTransactionEndpoint(s Service) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(*transferTransactionRequest)
		if req.To == req.From {
			return nil, e.ErrSameAccount
		}
		err := s.Transfer(ctx, req.Amount, req.To, req.From)
		if err != nil {
			return nil, err
		}

		return transaferTransactionResponse{Message: "success"}, nil
	}
}
