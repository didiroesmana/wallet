package service

import (
	"context"
	"errors"
	"testing"

	"bitbucket.org/didiroesmana/wallet/pkg/entity"
	e "bitbucket.org/didiroesmana/wallet/pkg/error"
	am "bitbucket.org/didiroesmana/wallet/pkg/mocks/account"
	tm "bitbucket.org/didiroesmana/wallet/pkg/mocks/transaction"
	"github.com/google/go-cmp/cmp"
)

func TestList(t *testing.T) {
	type args struct {
		ctx  context.Context
		page int
		size int
	}
	tests := []struct {
		name    string
		args    args
		want    []*entity.Transaction
		mockRes func() ([]*entity.Transaction, error)
		wantErr bool
		err     error
	}{
		{
			name: "success",
			args: args{
				ctx:  context.Background(),
				page: 1,
				size: 10,
			},
			want: []*entity.Transaction{
				{
					Account:   "test",
					Amount:    100,
					ToAccount: "test2",
					Direction: entity.OUTGOING_TRANSACTION,
				},
				{
					Account:     "test2",
					Amount:      100,
					FromAccount: "test1",
					Direction:   entity.INCOMING_TRANSACTION,
				},
			},
			mockRes: func() ([]*entity.Transaction, error) {
				return []*entity.Transaction{
					{
						Account:   "test",
						Amount:    100,
						ToAccount: "test2",
						Direction: entity.OUTGOING_TRANSACTION,
					},
					{
						Account:     "test2",
						Amount:      100,
						FromAccount: "test1",
						Direction:   entity.INCOMING_TRANSACTION,
					},
				}, nil
			},
			wantErr: false,
			err:     nil,
		},
		{
			name: "success_param_page_zero",
			args: args{
				ctx:  context.Background(),
				page: 0,
				size: 1000,
			},
			want: []*entity.Transaction{
				{
					Account:   "test",
					Amount:    100,
					ToAccount: "test2",
					Direction: entity.OUTGOING_TRANSACTION,
				},
				{
					Account:     "test2",
					Amount:      100,
					FromAccount: "test1",
					Direction:   entity.INCOMING_TRANSACTION,
				},
			},
			mockRes: func() ([]*entity.Transaction, error) {
				return []*entity.Transaction{
					{
						Account:   "test",
						Amount:    100,
						ToAccount: "test2",
						Direction: entity.OUTGOING_TRANSACTION,
					},
					{
						Account:     "test2",
						Amount:      100,
						FromAccount: "test1",
						Direction:   entity.INCOMING_TRANSACTION,
					},
				}, nil
			},
			wantErr: false,
			err:     nil,
		},
	}

	accMockRepo := &am.Repository{}
	trxMockRepo := &tm.Repository{}

	svc := New(trxMockRepo, accMockRepo)
	t.Run("ListFunction", func(t *testing.T) {
		for _, test := range tests {
			t.Run(test.name, func(tt *testing.T) {
				pageRepo := test.args.page
				if pageRepo <= 0 {
					pageRepo = 1
				}

				sizeRepo := test.args.size
				if sizeRepo <= 0 {
					sizeRepo = 10
				}

				trxMockRepo.On("List", test.args.ctx, pageRepo, sizeRepo).Return(test.mockRes())
				res, er := svc.List(test.args.ctx, test.args.page, test.args.size)
				if res != nil && test.want != nil {
					compRes := cmp.Diff(res, test.want)

					if compRes != "" {
						t.Fatalf("Case %s expected and res not same [%v]", test.name, compRes)
					}
				}

				if er != nil && test.err != nil {
					if er.Error() != test.err.Error() {
						tt.Errorf("List() = %v, want %v", er, test.err)
					}
				}
			})
		}
	})
}

func TestTransfer(t *testing.T) {
	type args struct {
		ctx    context.Context
		to     string
		from   string
		amount float64
	}

	accMockRepo := &am.Repository{}
	trxMockRepo := &tm.Repository{}
	ctx := context.Background()
	tests := []struct {
		name    string
		args    args
		want    error
		mockTrx func()
		mockAcc func()
	}{
		{
			name: "success",
			args: args{
				ctx:    ctx,
				from:   "test2",
				to:     "test",
				amount: 100,
			},
			want: nil,
			mockTrx: func() {
				trxMockRepo.On("Create", ctx, &entity.Transaction{
					Account:     "test2",
					Amount:      100,
					ToAccount:   "test",
					FromAccount: "",
					Direction:   entity.OUTGOING_TRANSACTION,
				}).Return(nil)
				trxMockRepo.On("Create", ctx, &entity.Transaction{
					Account:     "test",
					Amount:      100,
					FromAccount: "test2",
					ToAccount:   "",
					Direction:   entity.INCOMING_TRANSACTION,
				}).Return(nil)
			},
			mockAcc: func() {
				fromAcc := &entity.Account{
					ID:       "test",
					Balance:  1000,
					Currency: "USD",
				}
				accMockRepo.On("GetByID", ctx, "test").Return(fromAcc, nil)
				toAcc := &entity.Account{
					ID:       "test2",
					Balance:  1000,
					Currency: "USD",
				}
				accMockRepo.On("GetByID", ctx, "test2").Return(toAcc, nil)
				fromAcc.Balance -= 100
				accMockRepo.On("Update", ctx, fromAcc).Return(nil)
				toAcc.Balance += 100
				accMockRepo.On("Update", ctx, toAcc).Return(nil)
			},
		},
		{
			name: "fail_amount_below_zero",
			args: args{
				ctx:    ctx,
				from:   "test2",
				to:     "test",
				amount: -100,
			},
			want:    e.ErrInvalidAmount,
			mockAcc: func() {},
			mockTrx: func() {},
		},
		{
			name: "fail_from_account_not_found",
			args: args{
				ctx:    ctx,
				from:   "test3",
				to:     "test",
				amount: 100,
			},
			want: e.ErrAccNotFound,
			mockAcc: func() {
				accMockRepo.On("GetByID", ctx, "test3").Return(nil, e.ErrAccNotFound)
			},
			mockTrx: func() {},
		},
		{
			name: "fail_destination_account_not_found",
			args: args{
				ctx:    ctx,
				from:   "test",
				to:     "test4",
				amount: 100,
			},
			want: e.ErrAccNotFound,
			mockAcc: func() {
				accMockRepo.On("GetByID", ctx, "test4").Return(nil, e.ErrAccNotFound)
			},
			mockTrx: func() {},
		},
		{
			name: "fail_different_currency",
			args: args{
				ctx:    ctx,
				from:   "test-usd",
				to:     "test-eur",
				amount: 100,
			},
			want: e.ErrInvalidCurrency,
			mockAcc: func() {
				fromAcc := &entity.Account{
					ID:       "test-usd",
					Balance:  1000,
					Currency: "USD",
				}
				accMockRepo.On("GetByID", ctx, "test-usd").Return(fromAcc, nil)

				toAcc := &entity.Account{
					ID:       "test-eur",
					Balance:  1000,
					Currency: "EUR",
				}
				accMockRepo.On("GetByID", ctx, "test-eur").Return(toAcc, nil)
			},
			mockTrx: func() {},
		},
		{
			name: "fail_insufficient_funds",
			args: args{
				ctx:    ctx,
				from:   "test-insufficient",
				to:     "test",
				amount: 100,
			},
			want: e.ErrInsufficientBalance,
			mockTrx: func() {

			},
			mockAcc: func() {
				fromAcc := &entity.Account{
					ID:       "test-insufficient",
					Balance:  50,
					Currency: "USD",
				}
				accMockRepo.On("GetByID", ctx, "test-insufficient").Return(fromAcc, nil)
				toAcc := &entity.Account{
					ID:       "test2",
					Balance:  1000,
					Currency: "USD",
				}
				accMockRepo.On("GetByID", ctx, "test2").Return(toAcc, nil)
			},
		},
		{
			name: "fail_insufficient_funds_after_trx",
			args: args{
				ctx:    ctx,
				from:   "test-insufficient",
				to:     "test",
				amount: 50,
			},
			want: e.ErrMinimumBalance,
			mockTrx: func() {

			},
			mockAcc: func() {

			},
		},
		{
			name: "failed_update_balance_destination_account",
			args: args{
				ctx:    ctx,
				from:   "test20",
				to:     "test10",
				amount: 100,
			},
			want: errors.New("failed to update balance"),
			mockTrx: func() {

			},
			mockAcc: func() {
				fromAcc := &entity.Account{
					ID:       "test10",
					Balance:  1000,
					Currency: "USD",
				}
				accMockRepo.On("GetByID", ctx, "test10").Return(fromAcc, nil)
				toAcc := &entity.Account{
					ID:       "test20",
					Balance:  1000,
					Currency: "USD",
				}
				accMockRepo.On("GetByID", ctx, "test20").Return(toAcc, nil)
				toAcc.Balance += 100
				accMockRepo.On("Update", ctx, toAcc).Return(errors.New("failed to update balance"))
			},
		},
		{
			name: "failed_update_balance_destination_account",
			args: args{
				ctx:    ctx,
				from:   "test21",
				to:     "test11",
				amount: 100,
			},
			want: errors.New("failed to update balance"),
			mockTrx: func() {

			},
			mockAcc: func() {
				fromAcc := &entity.Account{
					ID:       "test11",
					Balance:  1000,
					Currency: "USD",
				}
				accMockRepo.On("GetByID", ctx, "test11").Return(fromAcc, nil)
				toAcc := &entity.Account{
					ID:       "test21",
					Balance:  1000,
					Currency: "USD",
				}
				accMockRepo.On("GetByID", ctx, "test21").Return(toAcc, nil)
				toAcc.Balance += 100
				accMockRepo.On("Update", ctx, toAcc).Return(nil)
				fromAcc.Balance -= 100
				accMockRepo.On("Update", ctx, fromAcc).Return(errors.New("failed to update balance"))
			},
		},
		{
			name: "failed_create_incoming_trx",
			args: args{
				ctx:    ctx,
				from:   "test200",
				to:     "test00",
				amount: 100,
			},
			want: errors.New("failed to create transaction"),
			mockTrx: func() {

				trxMockRepo.On("Create", ctx, &entity.Transaction{
					Account:     "test200",
					Amount:      100,
					ToAccount:   "test00",
					FromAccount: "",
					Direction:   entity.OUTGOING_TRANSACTION,
				}).Return(errors.New("failed to create transaction"))
				trxMockRepo.On("Create", ctx, &entity.Transaction{
					Account:     "test00",
					Amount:      100,
					FromAccount: "test200",
					ToAccount:   "",
					Direction:   entity.INCOMING_TRANSACTION,
				}).Return(errors.New("failed to create transaction"))
			},
			mockAcc: func() {
				fromAcc := &entity.Account{
					ID:       "test00",
					Balance:  1000,
					Currency: "USD",
				}
				accMockRepo.On("GetByID", ctx, "test00").Return(fromAcc, nil)
				toAcc := &entity.Account{
					ID:       "test200",
					Balance:  1000,
					Currency: "USD",
				}
				accMockRepo.On("GetByID", ctx, "test200").Return(toAcc, nil)
				fromAcc.Balance -= 100
				accMockRepo.On("Update", ctx, fromAcc).Return(nil)
				toAcc.Balance += 100
				accMockRepo.On("Update", ctx, toAcc).Return(nil)
			},
		},
		{
			name: "failed_create_outgoing_trx",
			args: args{
				ctx:    ctx,
				from:   "test201",
				to:     "test01",
				amount: 100,
			},
			want: errors.New("failed to create transaction"),
			mockTrx: func() {
				trxMockRepo.On("Create", ctx, &entity.Transaction{
					Account:     "test201",
					Amount:      100,
					ToAccount:   "test01",
					FromAccount: "",
					Direction:   entity.OUTGOING_TRANSACTION,
				}).Return(errors.New("failed to create transaction"))
				trxMockRepo.On("Create", ctx, &entity.Transaction{
					Account:     "test01",
					Amount:      100,
					FromAccount: "test201",
					ToAccount:   "",
					Direction:   entity.INCOMING_TRANSACTION,
				}).Return(nil)
			},
			mockAcc: func() {
				fromAcc := &entity.Account{
					ID:       "test01",
					Balance:  1000,
					Currency: "USD",
				}
				accMockRepo.On("GetByID", ctx, "test01").Return(fromAcc, nil)
				toAcc := &entity.Account{
					ID:       "test201",
					Balance:  1000,
					Currency: "USD",
				}
				accMockRepo.On("GetByID", ctx, "test201").Return(toAcc, nil)
				fromAcc.Balance -= 100
				accMockRepo.On("Update", ctx, fromAcc).Return(nil)
				toAcc.Balance += 100
				accMockRepo.On("Update", ctx, toAcc).Return(nil)
			},
		},
	}

	svc := New(trxMockRepo, accMockRepo)
	t.Run("TransferFunction", func(t *testing.T) {
		for _, test := range tests {
			t.Run(test.name, func(tt *testing.T) {
				test.mockAcc()
				test.mockTrx()
				er := svc.Transfer(test.args.ctx, test.args.amount, test.args.to, test.args.from)

				compRes := cmp.Diff(er, test.want, cmp.Comparer(func(x, y error) bool {
					if x == nil || y == nil {
						return x == nil && y == nil
					}
					return x.Error() == y.Error()
				}))

				if compRes != "" {
					t.Fatalf("Case %s expected and res not same [%v]", test.name, compRes)
				}
			})
		}
	})
}
