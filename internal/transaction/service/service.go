package service

import (
	"context"

	"bitbucket.org/didiroesmana/wallet/internal/account"
	"bitbucket.org/didiroesmana/wallet/internal/transaction"
	"bitbucket.org/didiroesmana/wallet/pkg/entity"
	e "bitbucket.org/didiroesmana/wallet/pkg/error"
)

type service struct {
	trxRepo transaction.Repository
	accRepo account.Repository
}

func New(trxRepo transaction.Repository, accRepo account.Repository) transaction.Service {
	return &service{
		trxRepo: trxRepo,
		accRepo: accRepo,
	}
}

func (s *service) List(ctx context.Context, page, size int) ([]*entity.Transaction, error) {
	if size <= 0 || size > 100 {
		size = 10
	}

	if page <= 0 {
		page = 1
	}

	return s.trxRepo.List(ctx, page, size)
}

func (s *service) Transfer(ctx context.Context, amount float64, to, from string) error {
	if amount <= 0 {
		return e.ErrInvalidAmount
	}

	// fetch accounts
	accFrom, err := s.accRepo.GetByID(ctx, from)
	if err != nil {
		return err
	}

	accTo, err := s.accRepo.GetByID(ctx, to)
	if err != nil {
		return err
	}

	// make sure currency is same
	if accFrom.Currency != accTo.Currency {
		return e.ErrInvalidCurrency
	}

	// make sure balance is enough
	if accFrom.Balance < amount {
		return e.ErrInsufficientBalance
	}

	// deduct from account
	accFrom.Balance -= amount
	// add to account
	accTo.Balance += amount

	if accFrom.Balance <= 0 {
		return e.ErrMinimumBalance
	}

	if err := s.accRepo.Update(ctx, accFrom); err != nil {
		return err
	}

	if err := s.accRepo.Update(ctx, accTo); err != nil {
		return err
	}

	// create incoming transaction
	inTrx := &entity.Transaction{
		Amount:      amount,
		Account:     to,
		FromAccount: from,
		Direction:   entity.INCOMING_TRANSACTION,
	}

	if err := s.trxRepo.Create(ctx, inTrx); err != nil {
		return err
	}

	// create outgoing transaction from source account
	outTrx := &entity.Transaction{
		Amount:    amount,
		Account:   from,
		ToAccount: to,
		Direction: entity.OUTGOING_TRANSACTION,
	}

	if err := s.trxRepo.Create(ctx, outTrx); err != nil {
		return err
	}

	return nil

}
