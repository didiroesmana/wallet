package entity

type Currency struct {
	ID   int    `json:"id"`
	Code string `json:"code"`
}
