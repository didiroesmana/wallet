package entity

const (
	INCOMING_TRANSACTION = "incoming"
	OUTGOING_TRANSACTION = "outgoing"
)

type Transaction struct {
	Account     string  `json:"account"`
	Amount      float64 `json:"amount"`
	FromAccount string  `json:"from_account,omitempty"`
	ToAccount   string  `json:"to_account,omitempty"`
	Direction   string  `json:"incoming"`
}
