package error

import "errors"

var (
	ErrCreateFailed    = errors.New("create failed")
	ErrUpdateFailed    = errors.New("update failed")
	ErrGetFailed       = errors.New("get failed")
	ErrInvalidArgument = errors.New("invalid argument")
	ErrAccountExist    = errors.New("account exist")
)
