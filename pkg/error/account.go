package error

import "errors"

var (
	ErrAccNotFound   = errors.New("account not found")
	ErrAccBalanceMin = errors.New("account balance cannot be less than 0")
)
