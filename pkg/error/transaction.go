package error

import "errors"

var (
	ErrInsufficientBalance = errors.New("insufficient balance")
	ErrMinimumBalance      = errors.New("remaining balance cannot be less than 0")
	ErrInvalidAmount       = errors.New("amount must be greater than 0")
	ErrInvalidCurrency     = errors.New("currency must be the same")
	ErrSameAccount         = errors.New("account cannot be the same")
)
