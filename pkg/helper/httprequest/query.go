package httprequest

import (
	"net/http"
	"strconv"
)

func ParsePagination(r *http.Request) (int, int) {
	vars := r.URL.Query()
	var p, s int = 1, 10
	page := vars.Get("page")
	if page == "" {
		p = 1
	} else {
		n, err := strconv.Atoi(page)
		if err != nil {
			return 0, 0
		}
		p = n
	}

	size := vars.Get("size")
	if size == "" {
		s = 10
	} else {
		n, err := strconv.Atoi(size)
		if err != nil {
			return 0, 0
		}
		s = n
	}

	return p, s
}
