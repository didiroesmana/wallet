# Wallet Service
This repository demonstrate how to build a wallet RESTful API in domain-driven design (DDD) with Go kit

## Build Image

to build the image, simply run 

`make image`

or if you prefer docker compose to run the service 


`docker compose up`

make sure to copy `.env.example` to `.env`


## Run Test

to run test 
`make test`



## API Documentation

Postman documentation [here](https://documenter.getpostman.com/view/19548478/UVeMH3aP)