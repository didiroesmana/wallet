package main

import (
	"database/sql"
	"fmt"
	"net/http"
	"os"
	"os/signal"
	"syscall"

	_ "github.com/lib/pq"

	"bitbucket.org/didiroesmana/wallet/internal/account"
	accountRepo "bitbucket.org/didiroesmana/wallet/internal/account/repository/postgresql"
	accountService "bitbucket.org/didiroesmana/wallet/internal/account/service"
	"bitbucket.org/didiroesmana/wallet/internal/transaction"
	transactionRepo "bitbucket.org/didiroesmana/wallet/internal/transaction/repository/postgresql"
	transactionService "bitbucket.org/didiroesmana/wallet/internal/transaction/service"
	"github.com/go-kit/kit/log"
)

func main() {

	var httpAddr = envString("HTTP_ADDR", ":8080")

	var logger log.Logger
	logger = log.NewLogfmtLogger(log.NewSyncWriter(os.Stderr))
	logger = log.With(logger, "ts", log.DefaultTimestampUTC)

	var ar account.Repository
	var as account.Service

	db := connectDB()
	ar = accountRepo.New(db)

	as = accountService.New(ar)

	tr := transactionRepo.New(db)
	ts := transactionService.New(tr, ar)

	mux := http.NewServeMux()

	mux.Handle("/v1/accounts", account.MakeHandler(as))
	mux.Handle("/v1/transactions", transaction.MakeHandler(ts))
	mux.Handle("/health", healthCheckHandler())

	http.Handle("/", accessControl(mux))

	errs := make(chan error, 2)
	go func() {
		logger.Log("transport", "http", "address", httpAddr, "msg", "listening")
		errs <- http.ListenAndServe(httpAddr, nil)
	}()
	go func() {
		c := make(chan os.Signal)
		signal.Notify(c, syscall.SIGINT)
		errs <- fmt.Errorf("%s", <-c)
	}()

	logger.Log("terminated", <-errs)
}

func envString(env, fallback string) string {
	e := os.Getenv(env)
	if e == "" {
		return fallback
	}
	return e
}

func accessControl(h http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Access-Control-Allow-Origin", "*")
		w.Header().Set("Access-Control-Allow-Methods", "GET, POST, OPTIONS")
		w.Header().Set("Access-Control-Allow-Headers", "Origin, Content-Type")

		if r.Method == "OPTIONS" {
			return
		}

		h.ServeHTTP(w, r)
	})
}

func healthCheckHandler() http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(http.StatusOK)
		w.Write([]byte("OK"))
	})
}

func connectDB() *sql.DB {
	cs := envString("DB_CONNECTION_STRING", "postgres://postgres:postgres@localhost:5432/wallet?sslmode=disable")
	db, err := sql.Open("postgres", cs)
	if err != nil {
		panic(err)
	}
	return db
}
